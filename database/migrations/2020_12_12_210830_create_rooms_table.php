<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
            $table->string('code')->uniqid()->index();
            $table->string('email')->index();
            $table->string('name')->index();
            $table->string('type');
            $table->string('os');
            $table->string('browser');
            $table->string('operator_name')->default('');
            $table->string('country');
            $table->boolean('is_online')->default(true);
            $table->timestamp('connected_at')->nullable(true);
            $table->timestamp('last_message')->nullable(true);
            $table->string('ip');
            $table->text('ip_info')->nullable(true);
            $table->bigInteger('user_id')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
