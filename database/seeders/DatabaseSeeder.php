<?php

namespace Database\Seeders;

use App\Models\Option;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Option::factory()->create();

        User::factory()->create([
            'email' => 'admin@livechat.com',
            'name' => 'Admin',
            'role' => config('const.default.admin'),
            'password' => Hash::make('password.live.2021')
        ]);
    }
}
