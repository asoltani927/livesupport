<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * User routes
 */
Route::post('login', App\Http\Controllers\Api\User\AuthController::class . '@login');
Route::middleware('auth:api')->group(function () {

    Route::get('options', \App\Http\Controllers\Api\User\SettingsController::class . "@index");
    Route::post('messages', \App\Http\Controllers\Api\User\MessageController::class . "@store");
    Route::get('messages', \App\Http\Controllers\Api\User\MessageController::class . "@index");
    Route::post('media', \App\Http\Controllers\Api\User\MediaController::class . "@store");
    Route::post('rooms/typing', \App\Http\Controllers\Api\User\RoomController::class . "@typing");
    Route::get('rooms', \App\Http\Controllers\Api\User\RoomController::class . "@show");
    Route::delete('rooms/leave', \App\Http\Controllers\Api\User\RoomController::class . "@leave");
    Route::put('messages/mark/read', \App\Http\Controllers\Api\User\MessageController::class . "@read");


});

/**
 * Admin routes
 */
Route::post('admin/login', App\Http\Controllers\Api\Admin\AuthController::class . '@login');
Route::middleware('auth:api')->prefix('admin')->group(function () {

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    Route::post('messages', \App\Http\Controllers\Api\Admin\MessageController::class . "@store");
    Route::get('messages', \App\Http\Controllers\Api\Admin\MessageController::class . "@index");
    Route::put('messages/mark/read', \App\Http\Controllers\Api\Admin\MessageController::class . "@read");

    Route::get('rooms', \App\Http\Controllers\Api\Admin\RoomController::class . "@index");
    Route::get('rooms/{rooms}', \App\Http\Controllers\Api\Admin\RoomController::class . "@show");
    Route::post('rooms/{rooms}/join', \App\Http\Controllers\Api\Admin\RoomController::class . "@join");
    Route::delete('rooms/{rooms}/leave', \App\Http\Controllers\Api\Admin\RoomController::class . "@leave");
    Route::post('rooms/{rooms}/typing', \App\Http\Controllers\Api\Admin\RoomController::class . "@typing");
    Route::get('options', \App\Http\Controllers\Api\Admin\SettingsController::class . "@index");
    Route::get('history', \App\Http\Controllers\Api\Admin\HistoryController::class . "@index");
    Route::put('options', \App\Http\Controllers\Api\Admin\SettingsController::class . "@update");
    Route::post('media', \App\Http\Controllers\Api\Admin\MediaController::class . "@store");
    Route::put('user/{userId}/offline', \App\Http\Controllers\Api\Admin\UserStatusController::class . "@offline");
    Route::put('user/{userId}/online', \App\Http\Controllers\Api\Admin\UserStatusController::class . "@online");
    Route::delete('user/offline', \App\Http\Controllers\Api\Admin\UserStatusController::class . "@allOffline");
});


