<?php

use App\Http\Controllers\Admin\HomeController as AdminHomeController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', HomeController::class . "@index")->name('login');
Route::get('/enter-seller', HomeController::class . "@seller");
Route::get('/enter-buyer', HomeController::class . "@buyer");
Route::get('/inbox', HomeController::class . "@index");
Route::get('/offline', HomeController::class . "@index");

Route::get('/watermelon', AdminHomeController::class . "@index");
Route::get('/watermelon/dashboard', AdminHomeController::class . "@index");
Route::get('/watermelon/multiple', AdminHomeController::class . "@index");
Route::get('/watermelon/history', AdminHomeController::class . "@index");
Route::get('/watermelon/settings', AdminHomeController::class . "@index");
Route::get('/watermelon/room/{room}', AdminHomeController::class . "@index");

