<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

//Broadcast::channel('App.Models.User.{id}', function ($user, $id) {
//    return (int) $user->id === (int) $id;
//});

Broadcast::channel('chat', function ($user) {
    return ['user' => $user];
});

Broadcast::channel('chatbox.messages.{user_id}', function ($user, $user_id) {
    if (auth()->check()) {
        $userId = $user_id;
        if ((int)$user->id === (int)$userId) {
            return ['id' => $user->id, 'name' => $user->name];
        }
        return ['id' => $user->id, 'name' => $user->name];
    }
    return false;
});

Broadcast::channel('chatbox.messages.admin.{user_id}', function ($user, $user_id) {
    if (auth()->check()) {
        $userId = $user_id;
        if ((int)$user->id === (int)$userId) {
            return ['id' => $user->id, 'name' => $user->name];
        }
        return ['id' => $user->id, 'name' => $user->name];
    }
    return false;
});

Broadcast::channel('chatbox.rooms.admin.typing.{user_id}', function ($user, $user_id) {
    if (auth()->check()) {
        $userId = $user_id;
        if ((int)$user->id === (int)$userId) {
            return ['id' => $user->id, 'name' => $user->name];
        }
        return ['id' => $user->id, 'name' => $user->name];
    }
    return false;
});

Broadcast::channel('chatbox.rooms.typing.{user_id}', function ($user, $user_id) {
    if (auth()->check()) {
        $userId = $user_id;
        if ((int)$user->id === (int)$userId) {
            return ['id' => $user->id, 'name' => $user->name];
        }
        return ['id' => $user->id, 'name' => $user->name];
    }
    return false;
});

Broadcast::channel('chatbox.rooms.admin.{user_id}', function ($user, $user_id) {
    if (auth()->check()) {
        $userId = $user_id;
        if ((int)$user->id === (int)$userId) {
            return ['id' => $user->id, 'name' => $user->name];
        }
        return ['id' => $user->id, 'name' => $user->name];
    }
    return false;
});
