<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ChatboxMessagesBroadcastEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $forAdmin;
    public $room;

    /**
     * Create a new event instance.
     *
     * @param $room
     * @param bool $forAdmin
     */
    public function __construct($room, $forAdmin = false)
    {
        //
        $this->room['id'] = $room['id'];
        $this->room['user_id'] = $room['user_id'];
        $this->forAdmin = $forAdmin;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        if ($this->forAdmin) {
            return new PrivateChannel('chatbox.messages.admin.' . config('const.default.admin'));
        } else {
            return new PrivateChannel('chatbox.messages.' . $this->room['user_id']);
        }
    }
}
