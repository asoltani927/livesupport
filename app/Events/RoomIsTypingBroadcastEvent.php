<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class RoomIsTypingBroadcastEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $room;
    /**
     * @var string
     */
    public $message;

    private $userId;

    /**
     * Create a new event instance.
     *
     * @param string $roomId
     * @param string $message
     * @param bool $userId
     */
    public function __construct(string $roomId, string $message, $userId = false)
    {
        //
        $this->room = (int)$roomId;
        $this->message = (string)$message;
        $this->userId = $userId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        if ($this->userId !== false) {
            return new PrivateChannel('chatbox.rooms.typing.' . $this->userId);
        } else {
            return new PrivateChannel('chatbox.rooms.admin.typing.' . config('const.default.admin'));
        }
    }
}
