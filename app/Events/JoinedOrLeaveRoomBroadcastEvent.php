<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class JoinedOrLeaveRoomBroadcastEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $room;
    /**
     * @var false|mixed
     */
    private $forAdmin;

    /**
     * Create a new event instance.
     * @param $room
     * @param bool $forAdmin
     */
    public function __construct($room, bool $forAdmin = false)
    {
        $this->room = $room;
        $this->forAdmin = $forAdmin;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        if ($this->forAdmin) {
            return new PrivateChannel('chatbox.messages.admin.' . config('const.default.admin'));
        } else {
            return new PrivateChannel('chatbox.messages.' . $this->room['user_id']);
        }
    }
}
