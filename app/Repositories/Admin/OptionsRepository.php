<?php

namespace App\Repositories\Admin;

use App\Models\Option;
use App\Tools\Support\Repository;

class OptionsRepository extends Repository
{
    protected $optionModel;

    public function __construct(Option $model)
    {
        $this->optionModel = $model;
    }

    public function getALl(): ?array
    {
        $model = $this->optionModel->get();
        if ($model) {
            return $model->toArray();
        }
        return null;
    }

    public function getValue($name): ?array
    {
        $model = $this->optionModel->where('name', '=', $name);
        $model = $model->first();
        if ($model)
            return $model->toArray();
        return null;
    }

    public function saveValue($name, $value = null): bool
    {
        $model = $this->optionModel->where('name', '=', $name);
        $model = $model->first();
        if ($model) {
            $model->value = $value;
            return $model->save();
        }
        return false;
    }

}
