<?php

namespace App\Repositories\Admin;

use App\Helpers\MediaUrl;
use App\Models\Message;
use App\Tools\Support\Repository;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;


class MessageRepository extends Repository
{
    protected $messageModel;

    public function __construct(Message $model)
    {
        $this->messageModel = $model;
    }

    /**
     * @param int $room
     * @param string $message
     * @param string|null $media
     * @param string $ip
     * @param string|null $as
     * @return null
     */
    public function create(int $room, string $message, string $media = null, string $ip = '0.0.0.0', string $as = null)
    {
        $model = $this->messageModel->create([
            'room_id' => $room,
            'user_id' => Auth::user()->id,
            'is_operator' => true,
            'ip' => $ip,
            'media_id' => $media,
            'content' => $message,
            'operator_as' => $as,
        ]);
        if ($model) {
            return $model->toArray();
        }
        return null;
    }

    /**
     * @param int $room
     * @param int $newer
     * @return array
     */
    public function getAll(int $room, $newer = 0): ?array
    {
        $model = $this->messageModel->with(['media']);
        $model = $model->where('room_id', '=', $room);
        if ($newer > 0) {
            $model = $model->where('id', '>', $newer);
        }
        $model = $model->orderby('id', 'desc')->paginate(50);
        $model = $model->toArray();
        $model = static::map($model['data'], [
            'id',
            'content',
            'media.url',
            'media.downloadable',
            'operator_as',
            'created_at',
            'is_operator',
        ]);
        $model = (array)array_reverse($model);
        return $model;
    }

    /**
     * @param string $search
     * @param string $fromDate
     * @param string $toDate
     * @param int $page
     * @return array
     */
    public function getByFilter(string $search, string $fromDate, string $toDate): ?array
    {
//        DB::enableQueryLog();
        $model = $this->messageModel->with(['room']);
        if (!empty($search)) {
            $model = $model->where(function ($query) use ($search) {
                $query = $query->where('content', 'like', '%' . $search . '%');
                $query = $query->orWhere('id', 'like', '%' . $search . '%');
                $query = $query->orWhereHas('room', function ($query2) use ($search) {
                    $query2 = $query2->where(function ($query3) use ($search) {
                        $query3 = $query3->orWhere('email', 'like', '%' . $search . '%');
                        $query3 = $query3->orWhere('name', 'like', '%' . $search . '%');
                        $query3 = $query3->orWhere('os', 'like', '%' . $search . '%');
                        $query3 = $query3->orWhere('ip', 'like', '%' . $search . '%');
                        $query3 = $query3->orWhere('code', 'like', '%' . $search . '%');
                        return $query3;
                    });
                    return $query2;
                });
                return $query;
            });
//            Log::info('----------------------------------');
        }
        if (!empty($fromDate)) {
            if (!empty($toDate)) {
                $model = $model->whereBetween('created_at', [
                    $fromDate . ' 00:00:00',
                    $toDate . ' 00:00:00',
                ]);
            } else {
                $model = $model->where('created_at', '>=', $fromDate . ' 00:00:00');
            }
        } else {
            if (!empty($toDate)) {
                $model = $model->where('created_at', '<=', $toDate . ' 00:00:00');
            }
        }
        $model = $model->orderby('id', 'desc')->paginate(100);
        $model = $model->toArray();
        $model['data'] = static::map($model['data'], [
            'id',
            'content',
            'room',
            'media.url',
            'media.downloadable',
            'created_at',
            'ip',
            'is_operator',
        ]);
//        $query = DB::getQueryLog();
//        Log::info('query', $query);
        return $model;
    }

    /**
     * @param int $id
     * @return array
     */
    public function getById(int $id): ?array
    {
        $model = $this->messageModel->with(['media']);
        $model = $model->where('id', '=', $id);
        $model = $model->first();
        if ($model) {
            $model = $model->toArray();
            $model = static::map([$model], [
                'id',
                'content',
                'media.url',
                'media.downloadable',
                'created_at',
                'operator_as',
                'is_operator',
            ]);
            return $model[0];
        }
        return null;
    }


    protected static function mapColumn($column, $record, &$newColumn = '')
    {
        switch ($column) {
            case 'media.url':
                if (isset($record['media']))
                    return MediaUrl::make($record['media']);
                return null;
                break;
            case 'media.downloadable':
                if (isset($record['media'])) {
                    if (in_array($record['media']['mime'], [
                        'image/png',
                        'image/jpeg',
                        'image/jpg',
                    ])) {
                        return false;
                    }
                    return true;
                }
                return false;
                break;
        }
        return parent::mapColumn($column, $record, $newColumn); // TODO: Change the autogenerated stub
    }

    public function markRead($room)
    {
        $this->messageModel->where('room_id', '=', $room)->where('is_operator', '=', false)->update([
            'read_at' => Carbon::now()
        ]);
        return true;
    }

}
