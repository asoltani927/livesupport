<?php

namespace App\Repositories\Admin;

use App\Models\User;
use App\Tools\Support\Repository;
use Illuminate\Support\Facades\Hash;

class UserRepository extends Repository
{
    protected $userModel;

    public function __construct(User $model)
    {
        $this->userModel = $model;
    }

    public function getById(int $id): ?array
    {
        $model = $this->userModel->where('id', '=', $id)->first();
        if ($model)
            return $model->toArray();
        return null;
    }
}
