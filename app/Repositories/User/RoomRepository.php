<?php

namespace App\Repositories\User;

use App\Models\Room;
use App\Tools\Support\Repository;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


class RoomRepository extends Repository
{
    protected $roomModel;

    public function __construct(Room $model)
    {
        $this->roomModel = $model;
    }

    public function getRoom(array $user, string $type, string $ip, string $country = null, $browser = '', $platform = '', $timezone = '', $ipInfo = [])
    {
        $model = $this->roomModel;
        $model = $model->where('user_id', '=', $user['id']);
        $model = $model->where('type', '=', $type);
        $model = $model->orderby('id', 'desc');
        $model = $model->whereDate('created_at', Carbon::today());
        $model = $model->first();

        if ($model) {

            $model->name = $user['name'];
            $model->os = $platform;
            $model->browser = $browser;
            $model->ip_info = json_encode($ipInfo);
            $model->country = (string)$country;
            $model->ip = $ip;
            $model->is_online = true;
            $model->timezone = $timezone;
            $model->save();

            $model = $model->toArray();

            return $model;
        }
        // TODO: user id hash
        $model = $this->roomModel->create([
            'code' => 'LCR-' . ($user['id']) . Str::random(10),
            'email' => $user['email'],
            'name' => $user['name'],
            'type' => $type,
            'os' => $platform,
            'browser' => $browser,
            'ip_info' => json_encode($ipInfo),
            'country' => (string)$country,
            'ip' => $ip,
            'user_id' => $user['id'],
            'is_online' => true,
            'timezone' => $timezone,
        ]);
        if ($model)
            return $model->toArray();
        return null;
    }

    public function updateLastMessage(): ?array
    {
        $model = $this->roomModel->where('user_id', '=', Auth::user()->id)->where('id', '=', Auth::user()->current_room)->first();
        if ($model) {
            $model->last_message = Carbon::now();
            $model->save();
            return $model->toArray();
        }
        return null;
    }

    public function getOne($room)
    {
        $model = $this->roomModel;
        $model = $model->where('id', '=', $room);
        $model = $model->first();
        if ($model) {
            $model = $model->toArray();
            $map = $this->getMap();
            if ($map) {
                $model = self::map([$model], $map);
                $model = $model[0];
                return $model;
            }
            return $model;
        }
        return null;
    }

    public function markOffline($roomId)
    {
        $model = $this->roomModel;
        $model = $model->where('id', '=', $roomId);
        $model = $model->update(
            [
                'is_online' => false,
            ]
        );
        return $model;
    }

    public function markOfflineWithUserId($userId)
    {
        $model = $this->roomModel;
        $model = $model->where('user_id', '=', $userId);
        $model = $model->update(
            [
                'is_online' => false,
            ]
        );
        return $model;
    }

    public function markOnline($roomId)
    {
        $model = $this->roomModel;
        $model = $model->where('id', '=', $roomId);
        $model = $model->update(
            [
                'is_online' => true,
            ]
        );
        return $model;
    }

    public function markAllOffline()
    {
        $model = $this->roomModel;
        $model = $model->where('is_online', '=', true);
        $model = $model->update(
            [
                'is_online' => false,
            ]
        );
        return $model;
    }

    public function markAsLeaveAdmin($room): bool
    {
        $model = $this->roomModel;
        $model = $model->where('id', '=', $room);
        $model = $model->first();
        if ($model) {
            $model->operator_name = '';
            $model->connected_at = null;
            $model->save();
            return true;
        }
        return false;
    }
}
