<?php

namespace App\Repositories\User;

use App\Models\User;
use App\Tools\Support\Repository;
use Illuminate\Support\Facades\Hash;

class UserRepository extends Repository
{
    protected $userModel;

    public function __construct(User $model)
    {
        $this->userModel = $model;
    }

    public function getByEmail(string $email)
    {
        $model = $this->userModel->where('email', '=', $email)->first();
        if ($model)
            return $model->toArray();
        return false;
    }

    public function createByEmail(string $email, string $name, string $type = 'buyer')
    {
        $password = config('const.default.password.buyer');
        if ($type === 'seller')
            $password = config('const.default.password.seller');
        $model = $this->userModel->create([
            'email' => $email,
            'name' => $name,
            'password' => Hash::make($password)
        ]);
        if ($model)
            return $model->toArray();
        return false;
    }
}
