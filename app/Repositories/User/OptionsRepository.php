<?php

namespace App\Repositories\User;

use App\Models\Option;
use App\Tools\Support\Repository;

class OptionsRepository extends Repository
{
    protected $optionModel;

    public function __construct(Option $model)
    {
        $this->optionModel = $model;
    }

    public function getALl(): ?array
    {
        $model = $this->optionModel->get();
        if ($model) {
            return $model->toArray();
        }
        return null;
    }

    public function getValue($name): ?array
    {
        $model = $this->optionModel->where('name', '=', $name);
        $model = $model->first();
        if ($model)
            return $model->toArray();
        return null;
    }
}
