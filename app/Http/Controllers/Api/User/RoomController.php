<?php

namespace App\Http\Controllers\Api\User;

use App\Events\JoinedOrLeaveRoomBroadcastEvent;
use App\Events\RoomIsTypingBroadcastEvent;
use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Repositories\User\RoomRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoomController extends Controller
{
    protected $roomRepository;

    public function __construct()
    {
        $this->roomRepository = new RoomRepository(new Room);
    }

    /**
     *
     * @param Request $request
     * @return JsonResponse
     *
     */
    public function typing(Request $request): JsonResponse
    {
        $message = $request->get('message', '');
        event(new RoomIsTypingBroadcastEvent(Auth::user()->current_room, (string)$message));
        return $this->response(ResponseStatus::HTTP_OK, '');
    }


    /**
     *
     * admin leave
     * @param Request $request
     * @return JsonResponse
     *
     */
    public function leave(Request $request): JsonResponse
    {
        $this->roomRepository->markAsLeaveAdmin(Auth::user()->current_room);
        $model = $this->roomRepository->getOne(Auth::user()->current_room);
        event(new JoinedOrLeaveRoomBroadcastEvent($model, true));
        return $this->response(ResponseStatus::HTTP_OK, '',$model);
    }


    public function show(Request $request): JsonResponse
    {
        $model = $this->roomRepository->setMap([
            'operator_name'
        ])->getOne(Auth::user()->current_room);
        return $this->response(ResponseStatus::HTTP_OK, '', $model);
    }
}
