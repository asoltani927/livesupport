<?php

namespace App\Http\Controllers\Api\User;

use App\Events\ChatboxRoomsBroadcastEvent;
use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Models\User;
use App\Repositories\User\RoomRepository;
use App\Repositories\User\UserRepository;
use hisorange\BrowserDetect\Parser as Browser;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    protected $userRepository;
    protected $roomRepository;


    public function __construct()
    {
        $this->userRepository = new UserRepository(new User);
        $this->roomRepository = new RoomRepository(new Room);
    }

    /**
     *
     * login to admin panel
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:256', // username is email address
            'name' => 'required|max:256',
            'type' => 'required|max:256',
            'timezone' => 'required|string|max:256',
            'password' => 'required|max:256',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $input = $request->all();
        $user = $this->userRepository->getByEmail($input['email']);
        if (!$user) {
            $user = $this->userRepository->createByEmail($input['email'], $input['name'], $input['type']);
        }
        if ($user) {
            if (Auth::attempt(['email' => $input['email'], 'password' => $input['password']])) {
                $user = Auth::user();
                $data['token'] = $user->createToken(strtolower(env('APP_NAME')))->accessToken;

                if ($user->name !== $input['name']) {
                    $user->name = $input['name'];
                    $user->save();
                }


                $data['user'] = [
                    'name' => $user->name,
                    'email' => $user->email,
                ];

                /**
                 * create room
                 */


                try {

                    $browser = Browser::browserName();
                    $platform = Browser::platformName();

                    $ipinfo = $request->ipinfo;
                    $ip = $request->ipinfo->ip;
                    $country = $request->ipinfo->country;
                    $country_name = $request->ipinfo->country_name;
                } catch (\Exception $ex) {
                    $ip = $request->getClientIp();
                    $country_name = 'unknown';
                }
                $room = $this->roomRepository->getRoom($user->toArray(), $input['type'], $ip, $country_name, $browser, $platform, $input['timezone'], $ipinfo);
                $data['room'] = $room;
                $user->current_room = $room['id'];
                $user->save();
                event(new ChatboxRoomsBroadcastEvent($room, 'connected'));
                return $this->response(
                    ResponseStatus::HTTP_OK,
                    'authorized',
                    $data,
                    []
                );
            }
            return $this->response(
                ResponseStatus::HTTP_UNAUTHORIZED,
                'The password is incorrect. Please double-check the password.'
            );
        } else {
            return $this->response(
                ResponseStatus::HTTP_INTERNAL_SERVER_ERROR,
                __('response.error')
            );
        }
    }
}
