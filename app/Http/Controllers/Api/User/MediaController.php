<?php

namespace App\Http\Controllers\Api\User;

use App\Helpers\ResponseMaker;
use App\Http\Controllers\Controller;
use App\Models\Media;
use App\Repositories\MediaRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MediaController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @todo get file size
     */
    public function store(Request $request): JsonResponse
    {
        //
        $file = $request->file('file');
        $uploadFolder = 'at-' . Auth::user()->id;
        $repository = new MediaRepository(new Media);
        $result = $repository->upload($file, $uploadFolder, true);
        if ($result['status'] === true) {
            $result = ResponseMaker::array($result)->toArray();
            return response()->json($result, 200);
        }
        $result = ResponseMaker::array($result)->toArray();
        return response()->json($result, 400);
    }


}
