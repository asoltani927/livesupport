<?php

namespace App\Http\Controllers\Api\User;

use App\Events\ChatboxMessagesBroadcastEvent;
use App\Helpers\Helper;
use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\Media;
use App\Models\Message;
use App\Models\Room;
use App\Repositories\MediaRepository;
use App\Repositories\User\MessageRepository;
use App\Repositories\User\RoomRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    protected $messageRepository;
    protected $roomsRepository;
    protected $mediaRepository;

    public function __construct()
    {
        $this->messageRepository = new MessageRepository(new Message);
        $this->roomsRepository = new RoomRepository(new Room());
        $this->mediaRepository = new MediaRepository(new Media);
    }

    //
    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $page = (int)$request->get('page', 1);
        $newer = (int)$request->get('newer', 0);
        $model = $this->messageRepository->getAll($page, $newer);
        return $this->response(ResponseStatus::HTTP_OK, '', $model);
    }

    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'message' => 'max:3096', // username is email address
            'media' => 'digits_between:1,999999999999999',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $input = $request->all();
        $message = null;
        $mediaId = null;
        if (isset($input['message']))
            $message = $input['message'];
        if (isset($input['media']) and $input['media'] > 0) {
            $mediaId = (int)$input['media'];
            // Check that The media belongs to The login user
            $media = $this->mediaRepository->getByIdAndUser($mediaId, Auth::user()->id);
            if (!$media) {
                return $this->response(
                    ResponseStatus::HTTP_METHOD_NOT_ALLOWED,
                    'The media field is required. '
                );
            }
        }
        // bot of them not be empty
        if (strlen($message) < 1 and $mediaId === null) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                'The message field is required. '
            );
        }
        $model = $this->messageRepository->create((string)$message, $mediaId, $request->getClientIp());
        if ($model) {
            $roomInfo = $this->roomsRepository->updateLastMessage();
            if ($roomInfo) {
                event(new ChatboxMessagesBroadcastEvent($roomInfo, true));
            }
            $model = $this->messageRepository->getById($model['id']);
            return $this->response(ResponseStatus::HTTP_OK, '', $model);
        }
        return $this->response(ResponseStatus::HTTP_METHOD_NOT_ALLOWED, '');
    }

    public function read(Request $request): \Illuminate\Http\JsonResponse
    {
        $model = $this->messageRepository->markRead(Auth::user()->current_room);
        if ($model) {
            return $this->response(ResponseStatus::HTTP_OK, '');
        }
        return $this->response(ResponseStatus::HTTP_METHOD_NOT_ALLOWED, '');

    }
}
