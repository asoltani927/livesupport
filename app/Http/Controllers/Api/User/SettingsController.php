<?php

namespace App\Http\Controllers\Api\User;

use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\Option;
use App\Repositories\User\OptionsRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class SettingsController extends Controller
{
    protected $optionRepository;

    public function __construct()
    {
        $this->optionRepository = new OptionsRepository(new Option);
    }

    protected function index(Request $request): JsonResponse
    {
        $model = $this->optionRepository->getAll();
        $data = [];
        foreach ($model as $key => $item) {
            $data[$item['name']] = $item['value'];
        }
        if (isset($data['isOffline']) and $data['isOffline'] === 'hour') {
            if (isset($data['startTime']) and isset($data['endTime'])) {
                $startTime = Carbon::now(config('app.timezone'))->format('Y-m-d') . ' ' . $data['startTime'] . ':00';
                $endTime = Carbon::now(config('app.timezone'))->format('Y-m-d') . ' ' . $data['endTime'] . ':00';
                $now = Carbon::now(config('app.timezone'))->format('Y-m-d H:i:s');
                if ($now < $startTime || $now >= $endTime) {
                    $data['isOffline'] = 'yes';
                } else {
                    $data['isOffline'] = 'no';
                }
            } else {
                $data['isOffline'] = 'no';
            }
        }
        return $this->response(
            ResponseStatus::HTTP_OK, '',
            $data);
    }

}
