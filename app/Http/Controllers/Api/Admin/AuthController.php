<?php

namespace App\Http\Controllers\Api\Admin;

use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Models\User;
use App\Repositories\User\RoomRepository;
use App\Repositories\User\UserRepository;
use hisorange\BrowserDetect\Parser as Browser;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    protected $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository(new User);
    }

    /**
     *
     * login to admin panel
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|email|max:256', // username is email address
            'password' => 'required|max:256',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $input = $request->all();
        if (Auth::attempt(['email' => $input['username'], 'password' => $input['password'], 'role' => 3])) {
            $user = Auth::user();
            $data['token'] = $user->createToken(strtolower(env('APP_NAME')))->accessToken;
            $data['user'] = [
                'name' => $user->name,
                'email' => $user->email,
            ];
            return $this->response(
                ResponseStatus::HTTP_OK,
                'authorized',
                $data,
                []
            );
        }
        return $this->response(
            ResponseStatus::HTTP_UNAUTHORIZED,
            'The password is incorrect.'
        );
    }
}
