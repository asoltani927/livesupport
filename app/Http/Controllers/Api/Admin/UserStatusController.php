<?php

namespace App\Http\Controllers\Api\Admin;

use App\Events\RoomIsStatusBroadcastEvent;
use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Models\User;
use App\Repositories\User\RoomRepository;
use App\Repositories\Admin\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserStatusController extends Controller
{
    protected $optionRepository;
    private $roomRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct()
    {
        $this->roomRepository = new RoomRepository(new Room);
        $this->userRepository = new UserRepository(new User);
    }

    public function allOffline(Request $request): JsonResponse
    {
        $this->roomRepository->markAllOffline();
        return $this->response(ResponseStatus::HTTP_OK, '');
    }

    public function offline(Request $request, $userId): JsonResponse
    {
        $user = $this->userRepository->getById($userId);
        // @todo if change admin and add role and permissions
        if (isset($user['id']) and $user['id'] !== config('const.default.admin') and $user['current_room'] !== null) {
            $this->roomRepository->markOffline($user['current_room']);
            $room = $this->roomRepository->setMap([
                'id',
                'operator_name'
            ])->getOne($user['current_room']);
            broadcast(new RoomIsStatusBroadcastEvent($room['id'], false));
//        broadcast(new JoinedOrLeaveRoomBroadcastEvent($room, true));
            return $this->response(ResponseStatus::HTTP_OK, '');
        } else if ($user['id'] === config('const.default.admin')) {
            return $this->response(ResponseStatus::HTTP_OK, '');
        } else if ($user['current_room'] === null) {
            $this->roomRepository->markOfflineWithUserId($user['id']);
        }
        return $this->response(ResponseStatus::HTTP_METHOD_NOT_ALLOWED, '');
    }

    public function online(Request $request, $userId): JsonResponse
    {
        $user = $this->userRepository->getById($userId);
        // @todo if change admin and add role and permissions
        if($user)
        {
            if (isset($user['id']) and $user['id'] !== config('const.default.admin') and $user['current_room'] !== null) {
                $this->roomRepository->markOnline($user['current_room']);
                $room = $this->roomRepository->setMap([
                    'id',
                    'operator_name'
                ])->getOne($user['current_room']);
                broadcast(new RoomIsStatusBroadcastEvent($room['id'], true));
//        broadcast(new JoinedOrLeaveRoomBroadcastEvent($room, true));
                return $this->response(ResponseStatus::HTTP_OK, '');
            } else if ($user['id'] === config('const.default.admin')) {
                return $this->response(ResponseStatus::HTTP_OK, '');
            }
        }
        return $this->response(ResponseStatus::HTTP_METHOD_NOT_ALLOWED, '');
    }
}
