<?php

namespace App\Http\Controllers\Api\Admin;

use App\Events\ChatboxMessagesBroadcastEvent;
use App\Helpers\Helper;
use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\Media;
use App\Models\Message;
use App\Models\Room;
use App\Repositories\Admin\RoomRepository;
use App\Repositories\MediaRepository;
use App\Repositories\Admin\MessageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HistoryController extends Controller
{
    protected $messageRepository;

    public function __construct()
    {
        $this->messageRepository = new MessageRepository(new Message);
    }

    //
    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $search = (string)$request->get('search');
        $from = (string)$request->get('fromDate', '');
        $to = (string)$request->get('toDate', '');
        $model = $this->messageRepository->getByFilter($search, $from, $to);
        return $this->response(ResponseStatus::HTTP_OK, '', $model);
    }
}
