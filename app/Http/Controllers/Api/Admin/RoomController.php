<?php

namespace App\Http\Controllers\Api\Admin;

use App\Events\JoinedOrLeaveRoomBroadcastEvent;
use App\Events\RoomIsTypingBroadcastEvent;
use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Repositories\Admin\RoomRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class RoomController extends Controller
{
    protected RoomRepository $roomRepository;

    public function __construct()
    {
        $this->roomRepository = new RoomRepository(new Room);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     *
     */
    public function index(Request $request): JsonResponse
    {
        $search = $request->get('search', '');
        $model = $this->roomRepository->getAll($search, 50, 'last_message');
        return $this->response(ResponseStatus::HTTP_OK, '', $model);
    }

    public function show(Request $request, $id): JsonResponse
    {
        $model = $this->roomRepository->getOne($id);
        //$model['is_online'] = Helper::isOnline($model['user_id']);
        return $this->response(ResponseStatus::HTTP_OK, '', $model);
    }

    public function join(Request $request, $id): JsonResponse
    {
        $operatorName = $request->get('name', '');
        $room = $this->roomRepository->getOne($id);
        if ($room) {
            if (!empty($room['operator_name'])) {
                $model = $room;
                $model['watcher'] = true;
            } else {
                $model = $this->roomRepository->join($id, $operatorName, $request->getClientIp());
//        $model['is_online'] = Helper::isOnline($model['user_id']);
                $model['watcher'] = false;
                event(new JoinedOrLeaveRoomBroadcastEvent($model));
                event(new JoinedOrLeaveRoomBroadcastEvent($model, true));
            }
            return $this->response(ResponseStatus::HTTP_OK, '', $model);
        }
        return $this->response(ResponseStatus::HTTP_FORBIDDEN, '');
    }

    public function leave(Request $request, $id): JsonResponse
    {
        $room = $this->roomRepository->getOne($id);
        $watcher = (bool)$request->get('watcher', false);

        $hashVerify = false;//Hash::check($request->getClientIp(), $room['operator_hash']);
        if ($watcher === false || $hashVerify) {
            $model = $this->roomRepository->leave($id);
//        $model['is_online'] = Helper::isOnline($model['user_id']);
            event(new JoinedOrLeaveRoomBroadcastEvent($model));
            event(new JoinedOrLeaveRoomBroadcastEvent($model, true));
        } else {
            $model = $room;
        }
        return $this->response(ResponseStatus::HTTP_OK, '', $model);
    }

    /**
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function typing(Request $request, $id): JsonResponse
    {
        $model = $this->roomRepository->getOne($id);
        event(new RoomIsTypingBroadcastEvent($id, '', $model['user_id']));
        return $this->response(ResponseStatus::HTTP_OK, '');
    }
}
