<?php

namespace App\Http\Controllers\Api\Admin;

use App\Events\ChatboxMessagesBroadcastEvent;
use App\Events\JoinedOrLeaveRoomBroadcastEvent;
use App\Helpers\Helper;
use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\Media;
use App\Models\Message;
use App\Models\Room;
use App\Repositories\Admin\RoomRepository;
use App\Repositories\MediaRepository;
use App\Repositories\Admin\MessageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    protected MessageRepository $messageRepository;
    protected RoomRepository $roomRepository;
    protected MediaRepository $mediaRepository;

    public function __construct()
    {
        $this->messageRepository = new MessageRepository(new Message);
        $this->mediaRepository = new MediaRepository(new Media);
        $this->roomRepository = new RoomRepository(new Room);
    }

    //
    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $room = (int)$request->get('room', '');
        $newer = (int)$request->get('newer', 0);
        if (empty($room))
            return $this->response(ResponseStatus::HTTP_METHOD_NOT_ALLOWED, '', []);
        $model = $this->messageRepository->getAll($room, $newer);
        return $this->response(ResponseStatus::HTTP_OK, '', $model);
    }

    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'message' => 'max:3096', // username is email address
            'as' => 'max:256', // username is email address
            'media' => 'digits_between:1,999999999999999',
            'room' => 'required|digits_between:1,999999999999999',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $input = $request->all();


        $message = null;
        $mediaId = null;
//        $operator_as = config('const.default.operator_as');
        $operator_as = $input['as'];

        $room = $this->roomRepository->find($input['room']);

        $joined_room = false;
        if ($room) {
            if (empty($room->operator_name)) {
                $model = $this->roomRepository->join($room->id, $operator_as, $request->getClientIp());
                event(new JoinedOrLeaveRoomBroadcastEvent($model));
                event(new JoinedOrLeaveRoomBroadcastEvent($model, true));
                $joined_room = true;
            }
        }


        if (isset($input['as']) and !empty($input['as'])) {
            $operator_as = $input['as'];
        }

        if (isset($input['message']))
            $message = $input['message'];
        if (isset($input['media']) and $input['media'] > 0) {
            $mediaId = $input['media'];

            // Check that The media belongs to The login user
            $media = $this->mediaRepository->getByIdAndUser($mediaId, Auth::user()->id);
            if (!$media) {
                return $this->response(
                    ResponseStatus::HTTP_METHOD_NOT_ALLOWED,
                    'The media field is required. '
                );
            }
        }
        // bot of them not be empty
        if (strlen($message) < 1 and $mediaId === null) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                'The message field is required. '
            );
        }
        $model = $this->messageRepository->create($input['room'], (string)$message, $mediaId, $request->getClientIp(), $operator_as);
        if ($model) {
            $roomInfo = $this->roomRepository->getOne($input['room']);
//            if (Helper::isOnline($roomInfo['user_id'])) {
            if ($roomInfo && (Helper::isOnline($roomInfo['user_id']) || $roomInfo['is_online']))
                event(new ChatboxMessagesBroadcastEvent($roomInfo));
//            }
            $model = $this->messageRepository->getById($model['id']);
            return $this->response(ResponseStatus::HTTP_OK, '', [
                'message' => $model,
                'joined' => $joined_room,
            ]);
        }
        return $this->response(ResponseStatus::HTTP_METHOD_NOT_ALLOWED, '');
    }

    public function read(Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'room' => 'required|digits_between:1,999999999999999',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $input = $request->all();
        $model = $this->messageRepository->markRead($input['room']);
        if ($model) {
            return $this->response(ResponseStatus::HTTP_OK, '');
        }
        return $this->response(ResponseStatus::HTTP_METHOD_NOT_ALLOWED, '');

    }
}
