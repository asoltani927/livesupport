<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseMaker;
use App\Helpers\ResponseStatus;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param status ResponseStatus
     * @param message string
     * @param array array of data
     */
    public function response(int $status, string $message, array $data = [], $errors = [])
    {
        $output = ResponseMaker::message($message);
        if (count($data) > 0)
            $output = $output->array($data);
        if (count($errors) > 0)
            $output = $output->errors($errors);
        $output = $output->toArray();
        return response()->json($output, $status);
    }
}
