<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index(Request $request): RedirectResponse
    {
        return redirect()->to('enter-seller');
    }

    public function seller(Request $request)
    {
        return view('seller');
    }

    public function buyer(Request $request)
    {
        return view('buyer');
    }
}
