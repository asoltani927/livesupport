<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'alt',
        'mime',
        'extension',
        'filename',
        'path',
        'real_path',
        'size',
        'type',
        'user_id',
        'thumbnails',
        'thumbnail_path',
    ];
}
