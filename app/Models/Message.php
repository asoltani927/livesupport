<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class Message extends Model
{
    use HasFactory;

    protected $fillable = [
        'room_id',
        'user_id',
        'is_operator',
        'ip',
        'operator_as',
        'media_id',
        'content',
    ];

    protected $casts = [
        'is_operator' => 'bool'
    ];

    public function media(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Media::class, 'media_id', 'id');
    }

    public function room(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Room::class, 'room_id', 'id');
    }

    public function getCreatedAtAttribute($date): string
    {
        if (!empty($date))
            return Carbon::parse($date)->tz(config('app.timezone'))->format('Y-m-d H:i:s');
        return '';
    }
}
