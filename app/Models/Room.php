<?php

namespace App\Models;

use hisorange\BrowserDetect\Exceptions\Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

class Room extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'email',
        'type',
        'name',
        'ip_info',
        'os',
        'browser',
        'country',
        'operator_name',
        'connected_at',
        'ip',
        'user_id',
        'last_message',
        'is_online',
        'timezone',
        'operator_ip',
        'operator_hash',
    ];

    protected $hidden = [
        'operator_ip',
    ];

    public function messages(): HasMany
    {
        return $this->hasMany(Message::class, 'room_id', 'id');
    }

    public function unread_messages(): HasMany
    {
        return $this->hasMany(Message::class, 'room_id', 'id')->where('read_at', '=', null)->where('is_operator', '=', false);
    }

    public function getIpInfoAttribute($data): array
    {
        try {
            $result = json_decode($data, true);
            if ($result)
                return $result;
            return [];
        } catch (Exception $ex) {
            return ['country' => ''];
        }
    }

    public function getCreatedAtAttribute($date): string
    {
        if (!empty($date))
            return Carbon::parse($date)->tz(config('app.timezone'))->format('Y-m-d H:i:s');
        return '';
    }

    public function getUpdatedAtAttribute($date): string
    {
        if (!empty($date))
            return Carbon::parse($date)->tz(config('app.timezone'))->format('Y-m-d H:i:s');
        return '';
    }

}
