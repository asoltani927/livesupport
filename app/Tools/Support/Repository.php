<?php

namespace App\Tools\Support;

use App\Tools\Interfaces\iRepository;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;


class Repository implements iRepository
{
    /**
     * @var array
     */
    protected $modelWith = [];
    protected $modelMap = [];

    public function setMap(array $map = []): Repository
    {
        $this->modelMap = $map;
        return $this;
    }

    protected function getMap(): array
    {
        return $this->modelMap;
    }

    public function setWith(array $with = []): Repository
    {
        $this->modelWith = $with;
        return $this;
    }

    protected function getWith($with = []): array
    {
        return $this->modelWith;
    }

    public static function map(array $records, array $columns): array
    {
        $data = [];
        foreach ($records as $key => $record) {
            // if columns set key => value use key as column name on result
            foreach ($columns as $colkey => $column) {
                if (Str::contains($column, '.')) {
                    $c = explode('.', $column);
                    if (isset($record[$c[0]][$c[1]])) {
                        $data[$key][$c[0]][$c[1]] = $record[$c[0]][$c[1]];
                    } else {
                        $mapColkey = '';
                        $colData = static::mapColumn($column, $record, $mapColkey);
                        if (empty($mapColkey)) {
                            if (is_numeric($colkey))
                                $mapColkey = $column;
                            else
                                $mapColkey = $colkey;
                        }
                        $data[$key][$c[0]][$c[1]] = $colData;
                    }
                } else {
                    if (array_key_exists($column, $record)) {
                        if ($record[$column] === null)
                            $data[$key][$column] = (string)$record[$column];
                        else
                            $data[$key][$column] = $record[$column];
                    } else {
                        $mapColkey = '';
                        $colData = static::mapColumn($column, $record, $mapColkey);
                        if (empty($mapColkey)) {
                            if (is_numeric($colkey))
                                $mapColkey = $column;
                            else
                                $mapColkey = $colkey;
                        }
                        $data[$key][$mapColkey] = $colData;
                    }
                }
            }
        }
        return $data;
    }

    protected static function mapColumn($column, $record, &$newColumn = '')
    {
        $newColumn = $column;
        return null;
    }
}
