<?php

namespace App\Tools\Interfaces;


interface iRepository {

    public static function map(array $records, array $columns) : array;
}
