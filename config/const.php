<?php

return [
    'version' => '1026',
    'default' => [
        'operator_as' => 'Hana',
        'password' => [
            'buyer' => 'e#bnd*g>bp',
            'seller' => 'u#vnd&g<bx',
        ],
        'admin' => 1,
    ],
];
