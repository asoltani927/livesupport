<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Social Tradia Live Chat</title>
    <link href="{{env('APP_URL')}}/assets/css/template.css" rel="stylesheet">
    <link href="{{env('APP_URL')}}/assets/css/template.dark.css" rel="stylesheet"
          media="(prefers-color-scheme: light)">

    <link rel="icon"
          type="image/png"
          href="/images/favicon.png">

          <link href="{{env('APP_URL')}}/css/app.css?v={{config('const.version')}}" rel="stylesheet">
</head>
<body>
<div id="app-admin">
</div>

<script src="{{env('APP_URL')}}/assets/js/libs/jquery.min.js"></script>
<script src="{{env('APP_URL')}}/assets/js/bootstrap/bootstrap.bundle.min.js"></script>
<script src="{{env('APP_URL')}}/assets/js/plugins/plugins.bundle.js"></script>
<script src="{{env('APP_URL')}}/assets/js/template.js"></script>


<script src="{{env('APP_URL')}}/js/app.js?v={{config('const.version')}}"></script>


<script type="text/javascript">

</script>
</body>
</html>
