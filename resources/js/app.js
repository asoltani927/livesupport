require('./bootstrap');

import Vue from 'vue';
import App from "./App.vue";
import './plugins'
import MainApp from "./App.vue";
import routes from './routes/user'
import routesAdmin from './routes/admin'
import store from './store'
import i18n from './plugins/i18n'

/**
 *
 * define layouts components
 *
 */
import authLayout from './layouts/auth.vue'
import userLayout from './layouts/user.vue'
import adminLayout from './layouts/admin.vue'

Vue.component('auth-layout', authLayout)
Vue.component('user-layout', userLayout)
Vue.component('admin-layout', adminLayout)
/******************************************** */


/**
 *
 * define user app
 *
 */

let sellerElement = document.getElementById("seller-app");
if (sellerElement) {
    window.getUserType = function () {
        return 'seller';
    }
    const app = new Vue({
        el: '#seller-app',
        components: {MainApp},
        method: {},
        router: routes,
        i18n,
        render: h => h(App),
        store
    });
} else {
    let buyerElement = document.getElementById("buyer-app");
    if (buyerElement) {
        window.getUserType = function () {
            return 'buyer';
        }
        const app = new Vue({
            el: '#buyer-app',
            components: {MainApp},
            method: {},
            router: routes,
            i18n,
            render: h => h(App),
            store
        });
    } else {

        /******************************************** */

        /**
         *
         * define admin app
         *
         */
        let adminElement = document.getElementById("app-admin");
        if (adminElement) {
            const appAdmin = new Vue({
                el: '#app-admin',
                components: {MainApp},
                method: {},
                router: routesAdmin,
                i18n,
                render: h => h(App),
                store
            });
        }

        /******************************************** */
    }
}
