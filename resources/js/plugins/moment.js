import Vue from 'vue';
import vue_moment from "vue-moment";

import moment_timezone from 'moment-timezone'

Vue.use(vue_moment,[
    moment_timezone
]);


