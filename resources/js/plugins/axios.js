import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
axios.defaults.baseURL = process.env.MIX_APP_API_URL;

const token = localStorage.getItem('token')
if (token) {
    axios.defaults.headers.common['Authorization'] = "Bearer " + token

}
axios.defaults.headers = {
    common: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    },
};

Vue.use(VueAxios, axios)
