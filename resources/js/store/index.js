import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import userAuth from './modules/user/auth.store';
import userMessage from './modules/user/messages.store';
import userMedia from './modules/user/media.store';
import userOptions from './modules/user/options.store';
import userRooms from './modules/user/rooms.store';


import adminAuth from './modules/admin/auth.store';
import adminRooms from './modules/admin/rooms.store';
import adminMessage from './modules/admin/messages.store';
import adminHistory from './modules/admin/hisotry.store';
import adminMedia from './modules/admin/media.store';
import adminOptions from './modules/admin/options.store';
import adminUserControl from './modules/admin/user.store';

export default new Vuex.Store({
    strict: true,
    modules: {
        userAuth,
        userMessage,
        userMedia,
        userOptions,
        userRooms,

        adminAuth,
        adminRooms,
        adminMessage,
        adminOptions,
        adminHistory,
        adminMedia,
        adminUserControl,
    },
});
