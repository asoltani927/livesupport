import axios from 'axios'
import catchHandle from '../../catch'
import Vue from "vue";

export default {

    name: 'messages',
    namespaced: true,

    state: {
        status: '',
        messages: [],
        data: [],
        message: '',
    },

    getters: {
        isLoading: state => {
            return state.status === 'loading';
        },
    },

    actions: {
        async create({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/messages', data: data, method: 'POST'})
                    .then((resp) => {
                        commit('success', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
        async markRead({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/messages/mark/read', data: data, method: 'PUT'})
                    .then((resp) => {
                        commit('success', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
        async index({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/messages?' + Vue.httpBuildQuery(data), method: 'GET'})
                    .then((resp) => {
                        commit('messages', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
    },

    mutations: {
        request(state) {
            state.status = 'loading'
        },
        success(state, data = []) {
            state.status = 'success'
            state.data = data
        },
        messages(state, data = []) {
            state.status = 'success'
            state.messages = data
        },
        error(state, message) {
            state.status = 'error'
            state.message = (message !== undefined) ? message : ''
        },
    },
};
