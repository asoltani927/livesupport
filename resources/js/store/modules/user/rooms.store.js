import axios from 'axios'
import catchHandle from '../../catch'
import Vue from "vue";

export default {

    name: 'rooms',
    namespaced: true,

    state: {
        status: '',
        data: [],
        current: {},
        message: '',
    },

    getters: {
        isLoading: state => {
            return state.status === 'loading';
        },
    },

    actions: {
        async typing({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/rooms/typing', data: data, method: 'POST'})
                    .then((resp) => {
                        commit('success', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
        async show({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/rooms', method: 'GET'})
                    .then((resp) => {
                        commit('current', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
        async leave({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/rooms/leave', method: 'DELETE'})
                    .then((resp) => {
                        commit('current', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
    },

    mutations: {
        request(state) {
            state.status = 'loading'
        },
        success(state, data = []) {
            state.status = 'success'
            state.data = data
        },
        current(state, data = []) {
            state.status = 'success'
            state.current = data
        },
        error(state, message) {
            state.status = 'error'
            state.message = (message !== undefined) ? message : ''
        },
    },
};
