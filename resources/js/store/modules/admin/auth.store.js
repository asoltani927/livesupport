import Vue from 'vue'
import axios from 'axios'
import catchHandle from '../../catch'

export default {

    name: 'auth',
    namespaced: true,


    state: {
        status: '',
        data: [],
        message: '',
        token: localStorage.getItem('token') || '',
        user: null,
    },

    getters: {
        isAuthenticated: state => {
            return !!state.token && state.user !== null;
        },
        adminInfo : state => {
            return state.user;
        }
    },

    actions: {
        async login({commit}, user) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/admin/login', data: user, method: 'POST'})
                    .then((resp) => {
                        const data = resp.data;
                        const token = data.token
                        const user = data.user
                        localStorage.setItem('token', token)
                        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
                        commit('success', {
                            user: user,
                            token: token,
                            message: data.message
                        })
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },

        async user({commit}) {

            const token = localStorage.getItem('token');
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token

            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/admin/user', method: 'GET'})
                    .then((resp) => {
                        const user = resp.data
                        commit('success', {
                            user: user,
                        })
                        resolve(resp)
                    })
                    .catch((err) => {
                        localStorage.removeItem('token')
                        axios.defaults.headers.common['Authorization'] = ''
                        commit('reset')
                        reject(err)
                    })
            })
        },

        async logout({commit}) {
            localStorage.removeItem('token')
            localStorage.removeItem('user')
            axios.defaults.headers.common['Authorization'] = ''
            commit('reset')
        },
    },

    mutations: {
        request(state) {
            state.status = 'loading'
        },
        //
        success(state, data = {token: '', user: '', message: ''}) {
            state.status = 'success'
            if (data.token !== undefined)
                state.token = data.token
            if (data.user !== undefined)
                state.user = data.user
            if (data.message !== undefined)
                state.message = data.message
        },
        //
        error(state, message) {
            state.status = 'error'
            state.message = (message !== undefined) ? message : ''
        },
        //
        reset(state) {
            state.status = ''
            state.message = ''
            state.token = ''
            state.user = null
        },
    },
};
