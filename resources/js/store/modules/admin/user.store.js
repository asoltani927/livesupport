import axios from 'axios'
import catchHandle from '../../catch'
import Vue from "vue";

export default {

    name: 'user',
    namespaced: true,

    state: {
        status: '',
        data: {},
        message: '',
    },

    getters: {
        isLoading: state => {
            return state.status === 'loading';
        },
    },

    actions: {
        async online({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/admin/user/' + data.id + '/online', method: 'PUT'})
                    .then((resp) => {
                        commit('success', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
        async offline({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/admin/user/' + data.id + '/offline', method: 'PUT'})
                    .then((resp) => {
                        commit('success', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
        async allOffline({commit}) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/admin/user/offline', method: 'DELETE'})
                    .then((resp) => {
                        commit('success', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
    },

    mutations: {
        request(state) {
            state.status = 'loading'
        },
        success(state, data = []) {
            state.status = 'success'
            state.data = data
        },
        error(state, message) {
            state.status = 'error'
            state.message = (message !== undefined) ? message : ''
        },
    },
};
