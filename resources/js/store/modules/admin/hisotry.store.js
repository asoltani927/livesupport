import axios from 'axios'
import catchHandle from '../../catch'
import Vue from "vue";

export default {

    name: 'history',
    namespaced: true,

    state: {
        status: '',
        messages: [],
        data: [],
        message: '',
    },

    getters: {
        isLoading: state => {
            return state.status === 'loading';
        },
    },

    actions: {
        async index({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/admin/history?'+ Vue.httpBuildQuery(data) , method: 'GET'})
                    .then((resp) => {
                        commit('success', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
    },

    mutations: {
        request(state) {
            state.status = 'loading'
        },
        success(state, data = []) {
            state.status = 'success'
            state.data = data
        },
        error(state, message) {
            state.status = 'error'
            state.message = (message !== undefined) ? message : ''
        },
    },
};
