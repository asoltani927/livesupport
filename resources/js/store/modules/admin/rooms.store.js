import axios from 'axios'
import catchHandle from '../../catch'
import Vue from "vue";

export default {

    name: 'rooms',
    namespaced: true,

    state: {
        status: '',
        data: [],
        records: [],
        message: '',
    },

    getters: {
        isLoading: state => {
            return state.status === 'loading';
        },
    },

    actions: {
        async index({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/admin/rooms?' + Vue.httpBuildQuery(data), method: 'GET'})
                    .then((resp) => {
                        commit('records', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
        async show({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/admin/rooms/' + data.id, data: Vue.httpBuildQuery(data), method: 'GET'})
                    .then((resp) => {
                        commit('success', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
        async join({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/admin/rooms/' + data.room + '/join', data: data, method: 'POST'})
                    .then((resp) => {
                        commit('success', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
        async leave({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/admin/rooms/' + data.room + '/leave', data: data, method: 'DELETE'})
                    .then((resp) => {
                        commit('success', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
        async typing({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/admin/rooms/' + data.room + '/typing', method: 'POST'})
                    .then((resp) => {
                        commit('success', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
    },

    mutations: {
        request(state) {
            state.status = 'loading'
        },
        success(state, data = []) {
            state.status = 'success'
            state.data = data
        },
        records(state, data = []) {
            state.status = 'success'
            state.records = data
        },
        error(state, message) {
            state.status = 'error'
            state.message = (message !== undefined) ? message : ''
        },
    },
};
