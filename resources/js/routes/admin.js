import Vue from 'vue'
import VueRouter, {RouteConfig} from 'vue-router'
import store from '../store'

import BaseComponent from "../views/Base";
import AdminLogin from "../views/admin/auth/login";
import AdminDashboard from "../views/admin/inbox";
import AdminSetting from "../views/admin/settings";
import AdminMultipleChat from "../views/admin/multiple";
import AdminHistoryChat from "../views/admin/history";
import AdminShowRoom from "../views/admin/rooms/show";


Vue.use(VueRouter)


const routes = [
    {
        path: '/watermelon',
        component: BaseComponent,
        children: [
            {
                path: '',
                name: 'admin-login',
                component: AdminLogin,
                meta: {layout: 'auth'},
            },
            {
                path: 'dashboard',
                name: 'admin-dashboard',
                component: AdminDashboard,
                meta: {
                    auth: true,
                    layout: 'admin'
                },
            },
            {
                path: 'multiple',
                name: 'admin-multiple',
                component: AdminMultipleChat,
                meta: {auth: true, layout: 'admin'},
            },
            {
                path: 'room/:room',
                name: 'admin-room',
                component: AdminShowRoom,
                meta: {auth: true, layout: 'admin'},
            },
            {
                path: 'history',
                name: 'admin-history',
                component: AdminHistoryChat,
                meta: {auth: true, layout: 'admin'},
            },
            {
                path: 'settings',
                name: 'admin-settings',
                component: AdminSetting,
                meta: {auth: true, layout: 'admin'},
            },
        ]
    },

];


const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    if (to.meta.auth) {
        store.dispatch('adminAuth/user').then(r => {
            if (!store.getters['adminAuth/isAuthenticated']) {
                next({
                    path: '/watermelon',
                    query: {redirect: to.fullPath}
                })
            } else {
                next()
            }
        }).catch(err => {
            next({
                path: '/watermelon',
                query: {redirect: to.fullPath}
            })
        });
        ;
    } else {
        next() // make sure to always call next()!
    }
})


export default router

