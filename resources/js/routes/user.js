import Vue from 'vue'
import VueRouter, {RouteConfig} from 'vue-router'
import store from '../store'

import BaseComponent from "../views/Base";
import UserLogin from "../views/user/auth/login";
import UserInbox from "../views/user/inbox";
import UserOffline from "../views/user/offline";


Vue.use(VueRouter)


const routes = [{
    path: '/',
    component: BaseComponent,
    children: [
        {
            path: '',
            name: 'user-login',
            component: UserLogin,
            meta: {layout: 'auth'},
        },
        {
            path: '/enter-seller',
            name: 'user-login',
            component: UserLogin,
            meta: {layout: 'auth'},
        },
        {
            path: '/enter-buyer',
            name: 'user-login',
            component: UserLogin,
            meta: {layout: 'auth'},
        },
        {
            path: 'inbox',
            name: 'user-inbox',
            component: UserInbox,
            meta: {auth: true, layout: 'user'},
        },
        {
            path: 'offline',
            name: 'user-offline',
            component: UserOffline,
            meta: {auth: false, layout: 'user'},
        },
    ]
},

];


const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {

    if (to.meta.auth) {
        store.dispatch('userAuth/user').then(r => {
            if (!store.getters['userAuth/isAuthenticated']) {
                next({
                    path: '/',
                    query: {redirect: to.fullPath}
                })
            } else {
                store.dispatch('userOptions/index').then((response) => {
                    if (response.data.isOffline === 'yes') {
                        next({
                            path: '/offline',
                        })
                    } else {
                        next()
                    }
                });
            }
        }).catch(err => {
            next({
                path: '/',
                query: {redirect: to.fullPath}
            })
        });
    } else {
        next() // make sure to always call next()!
    }
})


export default router
